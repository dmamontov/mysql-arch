CREATE SCHEMA sbtest;
CREATE USER sbtest@'%' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON sbtest.* to sbtest@'%';

CREATE database home;
USE home;

CREATE TABLE example (
     id serial PRIMARY KEY,
     col1 VARCHAR ( 50 ) UNIQUE NOT NULL,
     col2 VARCHAR ( 50 ) UNIQUE NOT NULL
);
INSERT INTO example VALUES( 1, 'a', 'd');
INSERT INTO example VALUES( 2, 'b', 'e');
INSERT INTO example VALUES( 3, 'c', 'f');
