Поднять сервис можно командой:

`docker-compose up `

Созданная база из инит-скрипта ссылка [link](https://gitlab.com/dmamontov/mysql-arch/-/blob/master/img/bd.png)
Изменённые параметры mysql [link](https://gitlab.com/dmamontov/mysql-arch/-/blob/master/img/poolsize.png)


Запуск sysbench
подготовка
```
docker run \
--rm=true \
--name=sb-prepare --network=custom_network \
severalnines/sysbench \
sysbench \
--db-driver=mysql \
--oltp-table-size=100000 \
--oltp-tables-count=24 \
--threads=1 \
--mysql-host=mysql-arch_otusdb_1 \
--mysql-port=3306 \
--mysql-user=sbtest \
--mysql-password=password \
/usr/share/sysbench/tests/include/oltp_legacy/parallel_prepare.lua \
run
```
тесты
```
docker run \
--name=sb-run --network=custom_network \
severalnines/sysbench \
sysbench \
--db-driver=mysql \
--report-interval=2 \
--mysql-table-engine=innodb \
--oltp-table-size=100000 \
--oltp-tables-count=24 \
--threads=64 \
--time=60 \
--mysql-host=mysql-arch_otusdb_1 \
--mysql-port=3306 \
--mysql-user=sbtest \
--mysql-password=password \
/usr/share/sysbench/tests/include/oltp_legacy/oltp.lua \
run
```
результаты нагрузки
[link](https://gitlab.com/dmamontov/mysql-arch/-/tree/master/results)
